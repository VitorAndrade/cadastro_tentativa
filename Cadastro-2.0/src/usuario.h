/*
 * Usuário.h
 *
 *  Created on: 1 de set de 2019
 *      Author: vitor
 */

#ifndef USUARIO_H_
#define USUARIO_H_

#define TAM_NOME 100
#define TAM_TIME 100

typedef struct {
	char nome[TAM_NOME];
	unsigned int id;
	char time[TAM_TIME];
} usuario;

usuario user_init(usuario u,char nome[TAM_NOME],unsigned int id,char time[TAM_TIME]);
void user_print(usuario u);
usuario user_copy(usuario u1,usuario u2); //copia de u2 para u1
int muda_id(unsigned int *id1,unsigned int id2);




#endif /* USUARIO_H_ */

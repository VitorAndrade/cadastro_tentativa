/*
 * Usuário.c
 *
 *  Created on: 1 de set de 2019
 *      Author: vitor
 */

#include <stdio.h>
#include <stdlib.h>
#include "usuario.h"

usuario user_init(usuario u,char nome[TAM_NOME],unsigned int id, char time[TAM_TIME])
{
	sprintf(u.nome,"%s",nome);
	u.id = id;
	sprintf(u.time,"%s",time);
	return u;

}

void user_print(usuario u)
{
	printf("---------------------------------\n");
	printf("nome: %s\n",u.nome);
	printf("id:%d\n",u.id);
	printf("time: %s\n",u.time);
	printf("\n---------------------------------\n");
}

usuario user_copy(usuario u1,usuario u2)
{

	sprintf(u1.time,"%s",u2.time);
	u1.id = u2.id;
	sprintf(u1.nome,"%s",u2.nome);

	return u1;

}
int muda_id(unsigned int *id1,unsigned int id2)
{
	*id1 = id2;

	return *id1;
}





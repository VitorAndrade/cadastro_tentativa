/*
 ============================================================================
 Name        : 0.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "usuario.h"

usuario u1, u2;
char nome[TAM_NOME] = "Geraldo";

int main(void) {

	u1 = user_init(u1,"Geraldo",1,"Tabajara");
	u2 = user_init(u1,"Allejo",2,"Brasil");

	u1.id = muda_id(&u1.id,3);

	//printf("%u",u1.id);

	user_print(u1);
	//user_print(u2);

	//user_print(u1);
	//user_print(u2);

	return EXIT_SUCCESS;
}
